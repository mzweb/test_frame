<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4880095e8832851c59a4f6df704fa15a
{
    public static $files = array (
        'e9745f1f3f053f8815511cc917c302a6' => __DIR__ . '/../..' . '/function.php',
    );

    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Mz\\' => 3,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Mz\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4880095e8832851c59a4f6df704fa15a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4880095e8832851c59a4f6df704fa15a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
